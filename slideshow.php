<section>
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <!-- select query  -->
            <?php
                $slide_query = "SELECT * FROM slideshows WHERE active = 1";
                $slides = $db->query($slide_query)->fetchAll();
            ?>
            <!-- open loop  -->
            <?php foreach($slides as $k => $slide){ ?>
            <div class="carousel-item <?php if($k==0) echo 'active'; ?>" style="height: 300px; overflow: hidden">
                <img src="<?php asset('assets/uploads/slideshow/'.$slide['photo']) ?>" 
                    class="d-block w-100" alt="..." 
                    style="height: 100%; object-fit: contain"
                >
            </div>
            <?php } ?>
           <!-- close loop  -->
        </div>
    </div>
</section>