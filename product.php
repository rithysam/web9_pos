<?php 
    include ('layouts/header.php');
?>

<?php
    include ('layouts/nav.php');
    $id = $_GET['cat'];
    $cat_query = "SELECT * FROM categories WHERE id = $id";
    $cat_name = $db->query($cat_query)->fetchArray();
   
?>
<p class="text-success mt-3">
    <?php echo 'View '.$cat_name['name']. ' cateogry' ?>
</p>
<section class="mt-3">

        <dvi class="row">
            <!-- select from product  -->
            <?php 
                $category_id = $_GET['cat'];
                $product_query = "SELECT * FROM products WHERE active = 1 AND category_id = $category_id ORDER BY id DESC";
                $products = $db->query($product_query)->fetchAll();
            ?>
            <!-- open loop product by category  -->
            <?php foreach($products as $product){ ?>
            <div class="col-sm-3 col-6">
                <div class="card" style="width: 100%;">
                   <div style="height: 200px; overflow: hidden;">
                    <img src="<?php asset('assets/uploads/product/'.$product['photo']) ?>" class="card-img-top" alt="..." style="height: 100%; object-fit: contain">
                   </div>
                    <div class="card-body">
                        <h5 class="text-center"><?php echo $product['name'] ?></h5>
                        <div class="row">
                            <?php if($product['discount'] > 0){ ?>
                                <div class="col text-center">
                                    <del><?php echo '$'.number_format($product['sale_price'], 2) ?></del>
                                </div>
                            <?php } ?>

                            <div class="col text-center">
                                <?php 
                                    if($product['discount'] > 0){
                                        $price =   $product['sale_price']-( ($product['sale_price'] * $product['discount']) / 100 );
                                        echo '<h4 class="text-danger">'.'$'.number_format($price, 2).'</h4>';
                                    }else{
                                        echo '<h4 class="text-danger">'.'$'.number_format($product['sale_price'], 2).'</h4>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <!-- close lopp  -->

        </dvi>
</section>

   





<?php 

    include ('layouts/footer.php');
?>
