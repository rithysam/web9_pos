<?php 
    include ('layouts/header.php');
?>

<?php
    include ('layouts/nav.php');

    include ('slideshow.php');
?>
   
<section class="mt-3">
    <!-- loop categories  -->
    <?php foreach( $categories as $category){ ?>
        <div class="row p-2">
            <div class="col bg-info">
                <h3 class="float-left"><?php echo $category['name'] ?></h3>
                <a href="product.php?cat=<?php echo $category['id'] ?>" class="btn btn-success float-right">View All</a>
            </div>
        </div>

        <dvi class="row">
            <!-- select from product  -->
            <?php 
                $category_id = $category['id'];
                $product_query = "SELECT * FROM products WHERE active = 1 AND category_id = $category_id LIMIT 4";
                $products = $db->query($product_query)->fetchAll();
            ?>
            <!-- open loop product by category  -->
            <?php foreach($products as $product){ ?>
            <div class="col-sm-3 col-6">
                <div class="card" style="width: 100%;">
                    <div style="height: 200px; overflow: hidden">
                        <img src="<?php asset('assets/uploads/product/'.$product['photo']) ?>" class="card-img-top" alt="..." style="height: 100%; object-fit: contain">
                    </div>
                    <div class="card-body">
                        <h5 class="text-center"><?php echo $product['name'] ?></h5>
                        <div class="row">
                            <?php if($product['discount'] > 0){ ?>
                                <div class="col text-center">
                                    <del><?php echo '$'.number_format($product['sale_price'], 2) ?></del>
                                </div>
                            <?php } ?>

                            <div class="col text-center">
                                <?php 
                                    if($product['discount'] > 0){
                                        $price =   $product['sale_price']-( ($product['sale_price'] * $product['discount']) / 100 );
                                        echo '<h4 class="text-danger">'.'$'.number_format($price, 2).'</h4>';
                                    }else{
                                        echo '<h4 class="text-danger">'.'$'.number_format($product['sale_price'], 2).'</h4>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <!-- close lopp  -->

        </dvi>

    <?php } ?>
</section>

   





<?php 

    include ('layouts/footer.php');
?>
