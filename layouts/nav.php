<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">
        <img src="<?php asset('assets/img/AdminLTELogo.png') ?>" width="30" height="30" class="d-inline-block align-top" alt="">
        Bootstrap
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
        <li class="nav-item <?php if(!isset($_GET['pg']) && !isset($_GET['cat']))  echo 'menu_active';  ?>">
            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown <?php if(isset($_GET['cat']))  echo 'menu_active';  ?>">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Category
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php
                    $categery_query = "SELECT * FROM categories WHERE active = 1";
                    $categories = $db->query($categery_query)->fetchAll();
                ?>
                
                <?php foreach($categories as $cat){ ?>
                <a href="product.php?cat=<?php echo $cat['id'] ?>" class="dropdown-item" ><?php echo $cat['name'] ?></a> 
                <?php } ?>
            </div>
        </li>
        <?php 
            $page_query = "SELECT * FROM pages WHERE active = 1";
            $pages = $db->query($page_query)->fetchAll();
        ?>
        <?php foreach($pages as $p){ ?>
        <li class="nav-item <?php if(isset($_GET['pg']) && $_GET['pg']== $p['id']) echo 'menu_active'; ?>">
            <a class="nav-link" href="page.php?pg=<?php echo $p['id'] ?>"><?php echo $p['name'] ?> </a>
        </li>
        <?php } ?>
        
       
        </ul>
    </div>
</nav>

<style>
    .menu_active{
        font-weight: bold;
        background: red !important;
    }
</style>