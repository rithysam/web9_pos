<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
    $query = "SELECT * FROM products WHERE active = 1";
    $products = $db->query($query)->fetchAll();
?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">New Stock</h3>
              </div>
             <!-- show message  -->
            <?php echo show_message(); ?>
            
              <form action="action/cut_stock.php" method="post">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-4">
                        <label for="name">Product Name</label>
                        <select name="product_id" id="product_id" class="form-control" required>
                            <option value="">--- Select a product ---</option>
                            <?php foreach($products as $pro){ ?>
                                <option value="<?php echo $pro['id'] ?>"> <?php echo $pro['code'].'-'.$pro['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="name">Product Quantity</label>
                        <input type="number" min='1' name="quantity" class="form-control" required>
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="name">Note</label>
                        <input type="text"name="note" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>