<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');

?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Stock In List</h3>

                <div class="card-tools">
                  <form>
                    <div class="input-group input-group-sm" style="width: 550px;">
                        <label>
                            Start Date: 
                            <input type="date" name="start_date" required>
                        </label>
                        <label>
                            End Date:
                            <input type="date" name="end_date" required>
                        </label>

                      <!-- <div class="input-group-append"> -->
                        <button type="submit" class="btn btn-xs btn-primary">
                          <i class="fas fa-search"></i> 
                          Filter
                        </button>
                      <!-- </div> -->
                    </div>
                  </form>
                </div>

                <br><br>

                <div class="row">
                    <div class="col">
                        <a href="create_stock.php" class="btn btn-primary">New Stock</a>
                    </div>
                </div>
              </div>
                    <?php 
                        $current_date = date('Y-m-d');
                        if(isset($_GET['start_date']) && isset($_GET['end_date'])){
                            $start_date = $_GET['start_date'];
                            $end_date = $_GET['end_date'];
                            $end_date = date('Y-m-d', strtotime($end_date."+ 1 day"));
                            $query = "SELECT 
                                  s.created_at,
                                  p.name as product_name,
                                  p.code as product_code,
                                  s.quantity_in,
                                  u.name as created_by,
                                  s.note
                                FROM stock_ins as s
                                INNER JOIN products as p ON p.id = s.product_id
                                INNER JOIN users as u ON u.id = s.user_id 
                                WHERE s.created_at BETWEEN '$start_date' AND '$end_date'
                                ORDER BY s.created_at
                              ";
                        }else{
                            $query = "SELECT 
                                  s.created_at,
                                  p.name as product_name,
                                  p.code as product_code,
                                  s.quantity_in,
                                  u.name as created_by,
                                  s.note
                                FROM stock_ins as s
                                INNER JOIN products as p ON p.id = s.product_id
                                INNER JOIN users as u ON u.id = s.user_id 
                                ORDER BY s.created_at
                              ";
                        }
                       
                        $stocks = $db->query($query)->fetchAll();
                    ?>
         
              <div class="card-body">
                
                <div class="row">
                    <div class="col">
                    <table class="table table-bordered table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Product Code</th>
                      <th>Product Name</th>
                      <th>Quantity</th>
                      <th>Note</th>
                      <th>Add By</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($stocks as $k => $s){ ?>
                      <tr>
                        <td><?php echo $k + 1 ?></td>
                        <td><?php echo $s['product_code'] ?></td>
                        <td><?php echo $s['product_name'] ?></td>
                        <td><?php echo $s['quantity_in'] ?></td>
                        <td><?php echo $s['note'] ?></td>
                        <td><?php echo $s['created_by'] ?></td>
                        <td><?php echo $s['created_at'] ?></td>
                      </tr>
                    <?php } ?>
                   
                  </tbody>
                </table>
                    </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>