  <?php 
    // header 
    include ('layouts/header.php');

    // top navigation 
    include ('layouts/nav.php');

    // sidebar 
    include ('layouts/sidebar.php');
    $date = date('Y-m-d');
    $current_month = date('m');

    $query_new_order = "SELECT * FROM orders WHERE created_at LIKE '%$date%'";
    $new_order = $db->query($query_new_order)->numRows();

    $query_income = "SELECT SUM((b.order_quantity * b.item_price) - ((b.order_quantity * b.item_price) * b.item_dicount/100) ) as total FROM orders as a
      INNER JOIN product_orders as b ON b.order_id = a.id
      WHERE created_at LIKE '%$date%'
      ";
    $income = $db->query($query_income)->fetchArray();

    $query_income_monthly = "SELECT SUM((b.order_quantity * b.item_price) - ((b.order_quantity * b.item_price) * b.item_dicount/100) ) as total FROM orders as a
      INNER JOIN product_orders as b ON b.order_id = a.id
      WHERE MONTH(created_at) = $current_month
      ";
    $income_monthly = $db->query($query_income_monthly)->fetchArray();

    $query_top_10 = "SELECT 
          b.product_id, c.name, count(b.product_id) as total
        FROM orders as a
        INNER JOIN product_orders as b ON b.order_id = a.id
        INNER JOIN products as c ON c.id = b.product_id 
        WHERE MONTH(a.created_at) = $current_month
        GROUP BY b.product_id
        ORDER BY total DESC
        LIMIT 10
      ";

      $top_10 = $db->query($query_top_10)->fetchAll();

  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content pt-3">
      <div class="container-fluid">
       
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $new_order ?></h3>

                <p>New Orders today</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><sup style="font-size: 20px">$</sup><?php echo number_format($income['total'], 2) ?></h3>

                <p>Income today</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
              <h3><sup style="font-size: 20px">$</sup><?php echo number_format($income_monthly['total'], 2) ?></h3>

                <p>Income this month</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Order this month</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-sm-6">
            <div class="card">
              <div class="cart-title p-3">
                Top 10 Order
              </div>
              <table class="table table-border">
              <thead>
                <tr>
                  <td>No.</td>
                  <td>Product Name</td>
                  <td>Number of order</td>
                </tr>
              </thead>
              <tbody>
                <?php foreach($top_10 as $k => $top){ ?>
                <tr>
                  <td><?php echo (int)$k+1 ?></td>
                  <td><?php echo $top['name'] ?></td>
                  <td><?php echo $top['total'] ?></td>
                </tr>

                <?php } ?>
              </tbody>
              </table>
            </div>
          </div>
        </div>
       
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  <?php 
     // footer 
     include ('layouts/footer.php');
  ?>