<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
    $id = $_GET['id'];
    $query = "SELECT * FROM categories WHERE id = $id";
    $user = $db->query($query)->fetchArray();
?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Edit User</h3>
              </div>
             
              <form action="action/action_edit.php" method="post">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" value="<?php echo $user['name'] ?>" class="form-control" required>
                        </div>
                       
                    </div>
                  
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>