<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
    $id = $_GET['id'];
    $query = "SELECT * FROM users WHERE id = $id";
    $user = $db->query($query)->fetchArray();
?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Edit User</h3>
              </div>
             
              <form action="action/action_edit.php" method="post">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="name">Name</label>
                            <input type="text" name="name" id="name" value="<?php echo $user['name'] ?>" class="form-control" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="gender">Gender</label>
                            <select name="gender" id="gender" class="form-control" required>
                                <option value="">-- Please choose an option --</option>
                                <option value="Male" <?php echo $user['gender'] == "Male" ? 'selected' : '';  ?>>Male</option>
                                <option value="Female" <?php echo $user['gender'] == "Female" ? 'selected' : '';  ?>>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" value="<?php echo $user['username'] ?>" class="form-control" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="phone" value="<?php echo $user['phone'] ?>" class="form-control">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="password">Email</label>
                            <input type="email" name="email" id="email" value="<?php echo $user['email'] ?>" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>