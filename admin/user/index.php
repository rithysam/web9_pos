<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');

?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <!-- show message  -->
            <?php echo show_message(); ?>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">User List</h3>

                <div class="card-tools">
                  <form>
                    <div class="input-group input-group-sm" style="width: 250px;">
                      <input type="text" name="search" value="<?php if(isset($_GET['search'])){echo $_GET['search'];} ?>" class="form-control float-right" placeholder="Search" required>

                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                        <?php
                          if(isset($_GET['search'])){
                        ?>
                          <a href="index.php" class="btn btn-default text-danger">X</a>
                        <?php
                          }
                        ?>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <div class="row p-3">
                    <div class="col-sm-12">
                        <a href="create.php" class="btn btn-primary">Create User</a>
                    </div>
                </div>
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Name</th>
                      <th>Gender</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if(isset($_GET['search'])){
                        $search = $_GET['search'];
                        $query = "SELECT * FROM users WHERE active = 1 AND (`name` LIKE '%$search%' OR `username` LIKE '%$search%' OR `email` LIKE '%$search%' OR `phone` LIKE '%$search%')";
                      }else{
                        $query = "SELECT * FROM users WHERE active = 1";
                      }
                        $users = $db->query($query)->fetchAll();
                    ?>
                    <?php foreach($users as $key => $user){ ?>
                    <tr>
                      <td><?php echo $key +1; ?></td>
                      <td><?php echo $user['username']; ?></td>
                      <td><?php echo $user['name']; ?></td>
                      <td><?php echo $user['gender']; ?></td>
                      <td><?php echo $user['email']; ?></td>
                      <td><?php echo $user['phone']; ?> </td>
                      <td>
                        <a href="edit.php?id=<?php echo $user['id'] ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
                        <a href="action/action_delete.php?id=<?php echo $user['id'] ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>