<?php

   session_start();
   date_default_timezone_set('Asia/Phnom_Penh');
    // define contant variable for root directory with project directory
    define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT'].'/web9/pos/');
    $base_url = 'http://localhost/web9/pos/';

    function asset($uri){
        global $base_url;
        echo $base_url . $uri;
    }

    function redirect($uri = ''){
        global $base_url;
        return $base_url . $uri;
    }

    function message($sms, $type='success'){
        if($type == 'success'){
            $message = '
                <div class="alert alert-success" role="alert">
                '.$sms.'
                </div>
            ';
        }elseif($type == 'error'){
            $message = '
            <div class="alert alert-danger" role="alert">
            '.$sms.'
            </div>
        ';
        }
        $_SESSION['message'] = $message;
    }

    function show_message(){
        if(isset($_SESSION['message'])){
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
    }

    function check_login(){
        if(!isset($_SESSION['user']) && !isset($_SESSION['logged_in'])){
      
            // get curren url 
            $actaul_link = '';
            if(isset($_SERVER['HTTPS'])){ 
                // check if https
                $actaul_link = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            }else{
                // not https (http)
                $actaul_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            }

            // redirect to login page if not login
            $url = redirect('index.php');
            if($url != $actaul_link ){
                message("Please login!", 'error');
                header("Location: $url");
            }
           
        }
    }




?>