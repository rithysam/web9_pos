  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php asset('dashboard.php') ?>" class="brand-link">
      <img src="<?php asset('assets/img/AdminLTELogo.png') ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">MINI POS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php asset('assets/img/user2-160x160.jpg') ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['user']['name'] ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            
            <li class="nav-item">
              <a href="<?php asset('admin/sale/index.php') ?>" class="nav-link">
                <i class="nav-icon fa fa-cart-arrow-down"></i>
                <p>
                    Sale
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?php asset('admin/order/index.php') ?>" class="nav-link">
                <i class="nav-icon fa fa-list"></i>
                <p>
                    Sale List
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?php asset('admin/product/index.php') ?>" class="nav-link">
                <i class="nav-icon fa fa-list"></i>
                <p>
                    Product
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-chart-pie"></i>
                <p>
                  Stock Management
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview" style="display: none;">
                <li class="nav-item">
                  <a href="<?php asset('admin/stock/stock-in.php') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Stock-In</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php asset('admin/stock/stock-out.php') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Stock-Out</p>
                  </a>
                </li>
              </ul>
            </li>
           
            <li class="nav-item">
              <a href="<?php asset('admin/slideshows/index.php') ?>" class="nav-link">
                <i class="nav-icon fa fa-images"></i>
                <p>
                    Slideshows
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?php asset('admin/pages/index.php') ?>" class="nav-link">
                <i class="nav-icon fa fa-file-alt"></i>
                <p>
                    Pages
                </p>
              </a>
            </li>

            <li class="nav-item">
                <a href="<?php asset('admin/category/index.php') ?>" class="nav-link">
                <i class="nav-icon fa fa-box-open"></i>
                <p>
                    Category
                </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="<?php asset('admin/user/index.php') ?>" class="nav-link">
                <i class="nav-icon fa fa-users"></i>
                <p>
                    Users
                </p>
                </a>
            </li>

            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-chart-pie"></i>
                <p>
                  Reports
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview" style="display: none;">
                <li class="nav-item">
                  <a href="<?php asset('admin/report/sale_report.php') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Sale Report</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php asset('admin/report/product_report.php') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Product Report</p>
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item">
                <a href="<?php asset('admin/auth/logout.php') ?>" class="nav-link">
                <i class="nav-icon fa fa-sign-out-alt"></i>
                <p>
                    Logout
                </p>
                </a>
            </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>