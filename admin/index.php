<?php 
    // header 
    include ('layouts/login_header.php');
    
    // check if already logged in 
    if(isset($_SESSION['user'])){
        header("Location: dashboard.php");
    }
?>

<div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <div class="card" style="margin-top: 100px">
            <div class="card-header">
                <h3 class="card-title text-center">MINI POS</h3>
            </div>

            <div class="card-body">
                <?php show_message(); ?>
                <form action="auth/action_login.php" method="post">
                    <div class="row">
                        <div class="form-group col">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" placeholder="Enter username" class="form-control" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="password">Password</label>
                            <input type="text" name="password" id="password" placeholder="Enter passowrd" class="form-control" required>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="form-group col">
                            <button class="btn btn-primary">Login</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-4"></div>
</div>

