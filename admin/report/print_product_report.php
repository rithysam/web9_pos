<?php 
    include ('../layouts/header.php');
?>

<?php 
    $current_date = date('Y-m-d');
    if(isset($_GET['start_date']) && isset($_GET['end_date']) && $_GET['category_id'] == ""){
        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];
        $end_date = date('Y-m-d', strtotime($end_date."+ 1 day"));
        $query = "SELECT 
                    b.product_id, c.name, count(b.product_id) as total, d.name as cat_name
                FROM orders as a
                INNER JOIN product_orders as b ON b.order_id = a.id
                INNER JOIN products as c ON c.id = b.product_id 
                INNER JOIN categories as d  ON d.id = c.category_id
                WHERE a.created_at BETWEEN '$start_date' and '$end_date'
                GROUP BY b.product_id 
                ORDER BY total DESC
                LIMIT 10
            ";
    }elseif(isset($_GET['start_date']) && isset($_GET['end_date']) && $_GET['category_id'] != ""){
        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];
        $end_date = date('Y-m-d', strtotime($end_date."+ 1 day"));
        $cat_id = $_GET['category_id'];
        $query = "SELECT 
                    b.product_id, c.name, count(b.product_id) as total, d.name as cat_name
                FROM orders as a
                INNER JOIN product_orders as b ON b.order_id = a.id
                INNER JOIN products as c ON c.id = b.product_id 
                INNER JOIN categories as d  ON d.id = c.category_id
                WHERE a.created_at BETWEEN '$start_date' and '$end_date'
                AND c.category_id = $cat_id
                GROUP BY b.product_id 
                ORDER BY total DESC
                LIMIT 10
            ";
    }else{
        $query = "SELECT 
                b.product_id, c.name, count(b.product_id) as total, d.name as cat_name
            FROM orders as a
            INNER JOIN product_orders as b ON b.order_id = a.id
            INNER JOIN products as c ON c.id = b.product_id 
            INNER JOIN categories as d  ON d.id = c.category_id
            GROUP BY b.product_id 
            ORDER BY total DESC
            LIMIT 10
        ";
    }
    
    $top10 = $db->query($query)->fetchAll();
    
?>

<!-- header  -->
<a href="product_report.php" class="btn btn-xs btn-success btn_print">Go Back</a>
<section>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="text-center">Tela Mart</h3>
            <hr>
            <h3 class="text-center">Top 10 Product  Report</h3>
        </div>
    </div>
</section>

<div class="row">
    <div class="col-sm-12">
        <table class="table">
            <tr>
                <th>No</th>
                <th>Category Name</th>
                <th>Product Name</th>
                <th>Number of Sale</th>
            </tr>

            <?php 
                foreach($top10 as $key => $value){
            ?>
                <tr>
                    <td><?php echo $key+1 ?></td>
                    <td><?php echo $value['cat_name'] ?></td>
                    <td><?php echo $value['name'] ?></td>
                    <td><?php echo $value['total'] ?></td>
                </tr>
            <?php
                }
            ?>
        </table>
    </div>
</div>


<!-- footer  -->
<section>
    <div class="row">
        <div class="col"></div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col">
            Date: <?php echo date('d/m/Y') ?>
            <br>
            Report By : <?php echo $_SESSION['user']['name']; ?>

            <br><br>

            ..................................................................

        </div>
    </div>
</section>
<style>
    @media print{
        .btn_print{
            display: none;
        }
    }
</style>
<script>
    window.print();
</script>
