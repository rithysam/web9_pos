<?php 
    include ('../layouts/header.php');
?>

<?php 
    $current_date = date('Y-m-d');
    if(isset($_GET['start_date']) && isset($_GET['end_date'])){
        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];
        $query = "SELECT 
            c.name as `user_name`,
            a.order_code,
            a.created_at,
            SUM((b.order_quantity * b.item_price) - (((b.order_quantity * b.item_price) * b.item_dicount) / 100 )) as total_price
        FROM orders as a 
        INNER JOIN product_orders as b ON b.order_id = a.id
        INNER JOIN users as c ON c.id = a.user_id
        WHERE  a.created_at BETWEEN '$start_date' AND '$end_date'
        GROUP BY a.id ";
    }else{
        $query = "SELECT 
            c.name as `user_name`,
            a.order_code,
            a.created_at,
            SUM((b.order_quantity * b.item_price) - (((b.order_quantity * b.item_price) * b.item_dicount) / 100 )) as total_price
        FROM orders as a 
        INNER JOIN product_orders as b ON b.order_id = a.id
        INNER JOIN users as c ON c.id = a.user_id
        WHERE  a.created_at LIKE '%$current_date%'
        GROUP BY a.id ";
    }
    
    $sales = $db->query($query)->fetchAll();
    $total_income = 0;
?>
<!-- header  -->
<a href="sale_report.php" class="btn btn-xs btn-success btn_print">Go Back</a>
<section>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="text-center">Tela Mart</h3>
            <hr>
            <h3 class="text-center">Sale Report</h3>
        </div>
    </div>
</section>

<table class="table table-bordered table-hover text-nowrap">
    <thead>
    <tr>
        <th>No</th>
        <th>Purchaser</th>
        <th>Order Code</th>
        <th>Order Date</th>
        <th>Total Price</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($sales as $key => $sale){ ?>
    <tr>
        <td><?php echo $key +1; ?></td>
        <td><?php echo $sale['user_name']; ?></td>
        <td><?php echo $sale['order_code']; ?></td>
        <td><?php echo $sale['created_at']; ?></td>
        <td><?php echo '$'.number_format($sale['total_price'], 2); ?></td>
        <?php 
            $total_income = $total_income + $sale['total_price'];
        ?>
    </tr>
    <?php } ?>
    <tr class="bg-warning">
        <td colspan="4" class="text-right">Total Income</td>
        <td><?php echo '$'.number_format($total_income, 2) ?></td>
    </tr>
    </tbody>
</table>

<!-- footer  -->
<section>
    <div class="row">
        <div class="col"></div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col">
            Date: <?php echo date('d/m/Y') ?>
            <br>
            Report By : <?php echo $_SESSION['user']['name']; ?>

            <br><br>

            ..................................................................

        </div>
    </div>
</section>
<style>
    @media print{
        .btn_print{
            display: none;
        }
    }
</style>
<script>
    window.print();
</script>
