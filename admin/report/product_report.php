<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
    $subject = ["PHP", "MySQL", "HTML",
"CSS", "JavaScript"];

var_dump($subject);

?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Top 10 selling product report</h3>

                <div class="card-tools">
                  <form>
                    <div class="input-group input-group-sm" style="width: 750px;">
                        <label>
                            <?php
                                $cat_query = "SELECT * FROM categories WHERE active = 1";
                                $cats = $db->query($cat_query)->fetchAll();
                            ?>
                            <select name="category_id">
                                <option value="">--Choose category--</option>
                                <?php
                                    foreach($cats as $cat){
                                ?>
                                    <option value="<?php echo $cat['id'] ?>"><?php echo $cat['name'] ?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </label>
                        <label>
                            Start Date: 
                            <input type="date" name="start_date" value="<?php echo date('Y-m-d') ?>" required>
                        </label>
                        <label>
                            End Date:
                            <input type="date" name="end_date" value="<?php echo date('Y-m-d') ?>" required>
                        </label>

                      <!-- <div class="input-group-append"> -->
                        <button type="submit" class="btn btn-xs btn-primary">
                          <i class="fas fa-search"></i> 
                          Filter
                        </button>
                      <!-- </div> -->
                    </div>
                  </form>
                </div>
              </div>
                    <?php 
                        $current_date = date('Y-m-d');
                        if(isset($_GET['start_date']) && isset($_GET['end_date']) && $_GET['category_id'] == ""){
                            $start_date = $_GET['start_date'];
                            $end_date = $_GET['end_date'];
                            $end_date = date('Y-m-d', strtotime($end_date."+ 1 day"));
                            $query = "SELECT 
                                        b.product_id, c.name, count(b.product_id) as total, d.name as cat_name
                                    FROM orders as a
                                    INNER JOIN product_orders as b ON b.order_id = a.id
                                    INNER JOIN products as c ON c.id = b.product_id 
                                    INNER JOIN categories as d  ON d.id = c.category_id
                                    WHERE a.created_at BETWEEN '$start_date' and '$end_date'
                                    GROUP BY b.product_id 
                                    ORDER BY total DESC
                                    LIMIT 10
                                ";
                        }elseif(isset($_GET['start_date']) && isset($_GET['end_date']) && $_GET['category_id'] != ""){
                            $start_date = $_GET['start_date'];
                            $end_date = $_GET['end_date'];
                            $end_date = date('Y-m-d', strtotime($end_date."+ 1 day"));
                            $cat_id = $_GET['category_id'];
                            $query = "SELECT 
                                        b.product_id, c.name, count(b.product_id) as total, d.name as cat_name
                                    FROM orders as a
                                    INNER JOIN product_orders as b ON b.order_id = a.id
                                    INNER JOIN products as c ON c.id = b.product_id 
                                    INNER JOIN categories as d  ON d.id = c.category_id
                                    WHERE a.created_at BETWEEN '$start_date' and '$end_date'
                                    AND c.category_id = $cat_id
                                    GROUP BY b.product_id 
                                    ORDER BY total DESC
                                    LIMIT 10
                                ";
                        }else{
                            $query = "SELECT 
                                    b.product_id, c.name, count(b.product_id) as total, d.name as cat_name
                                FROM orders as a
                                INNER JOIN product_orders as b ON b.order_id = a.id
                                INNER JOIN products as c ON c.id = b.product_id 
                                INNER JOIN categories as d  ON d.id = c.category_id
                                GROUP BY b.product_id 
                                ORDER BY total DESC
                                LIMIT 10
                            ";
                        }
                       
                        $top10 = $db->query($query)->fetchAll();
                       
                    ?>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <div class="row p-3">
                    <div class="col-sm-12">
                        <?php
                            if(isset($_GET['start_date']) && isset($_GET['end_date'])){
                                echo '<a href="print_product_report.php?category_id='.$_GET['category_id'].'&start_date='.$start_date.'&end_date='.$end_date.'" class="btn btn-primary"> <i class="fa fa-print"></i> Print</a>';
                            }else{
                                echo '<a href="print_product_report.php" class="btn btn-primary"> <i class="fa fa-print"></i> Print</a>';
                            }

                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <table class="table">
                           <tr>
                                <th>No</th>
                                <th>Category Name</th>
                                <th>Product Name</th>
                                <th>Number of Sale</th>
                           </tr>

                           <?php 
                                foreach($top10 as $key => $value){
                            ?>
                                <tr>
                                    <td><?php echo $key+1 ?></td>
                                    <td><?php echo $value['cat_name'] ?></td>
                                    <td><?php echo $value['name'] ?></td>
                                    <td><?php echo $value['total'] ?></td>
                                </tr>
                            <?php
                                }
                           ?>
                        </table>
                    </div>
                </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>