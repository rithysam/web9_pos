<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');

?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Sale Report</h3>

                <div class="card-tools">
                  <form>
                    <div class="input-group input-group-sm" style="width: 550px;">
                        <label>
                            Start Date: 
                            <input type="date" name="start_date" required>
                        </label>
                        <label>
                            End Date:
                            <input type="date" name="end_date" required>
                        </label>

                      <!-- <div class="input-group-append"> -->
                        <button type="submit" class="btn btn-xs btn-primary">
                          <i class="fas fa-search"></i> 
                          Filter
                        </button>
                      <!-- </div> -->
                    </div>
                  </form>
                </div>
              </div>
                    <?php 
                        $current_date = date('Y-m-d');
                        if(isset($_GET['start_date']) && isset($_GET['end_date'])){
                            $start_date = $_GET['start_date'];
                            $end_date = $_GET['end_date'];
                            $end_date = date('Y-m-d', strtotime($end_date."+ 1 day"));
                            $query = "SELECT 
                                c.name as `user_name`,
                                a.order_code,
                                a.created_at,
                                SUM((b.order_quantity * b.item_price) - (((b.order_quantity * b.item_price) * b.item_dicount) / 100 )) as total_price
                            FROM orders as a 
                            INNER JOIN product_orders as b ON b.order_id = a.id
                            INNER JOIN users as c ON c.id = a.user_id
                            WHERE  a.created_at BETWEEN '$start_date' AND '$end_date'
                            GROUP BY a.id ";
                        }else{
                            $query = "SELECT 
                                c.name as `user_name`,
                                a.order_code,
                                a.created_at,
                                SUM((b.order_quantity * b.item_price) - (((b.order_quantity * b.item_price) * b.item_dicount) / 100 )) as total_price
                            FROM orders as a 
                            INNER JOIN product_orders as b ON b.order_id = a.id
                            INNER JOIN users as c ON c.id = a.user_id
                            WHERE  a.created_at LIKE '%$current_date%'
                            GROUP BY a.id ";
                        }
                       
                        $sales = $db->query($query)->fetchAll();
                        $total_income = 0;
                    ?>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <div class="row p-3">
                    <div class="col-sm-12">
                        <?php
                            if(isset($_GET['start_date']) && isset($_GET['end_date'])){
                                echo '<a href="print_sale_report.php?start_date='.$start_date.'&end_date='.$end_date.'" class="btn btn-primary"> <i class="fa fa-print"></i> Print</a>';
                            }else{
                                echo '<a href="print_sale_report.php" class="btn btn-primary"> <i class="fa fa-print"></i> Print</a>';
                            }

                        ?>
                    </div>
                </div>
                <table class="table table-bordered table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Purchaser</th>
                      <th>Order Code</th>
                      <th>Order Date</th>
                      <th>Total Price</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php foreach($sales as $key => $sale){ ?>
                    <tr>
                        <td><?php echo $key +1; ?></td>
                        <td><?php echo $sale['user_name']; ?></td>
                        <td><?php echo $sale['order_code']; ?></td>
                        <td><?php echo $sale['created_at']; ?></td>
                        <td><?php echo '$'.number_format($sale['total_price'], 2); ?></td>
                        <?php 
                            $total_income = $total_income + $sale['total_price'];
                        ?>
                    </tr>
                    <?php } ?>
                    <tr class="bg-warning">
                        <td colspan="4" class="text-right">Total Income</td>
                        <td><?php echo '$'.number_format($total_income, 2) ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>