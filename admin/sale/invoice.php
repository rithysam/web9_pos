<?php 
    include ('../layouts/header.php');
    $order_id = $_GET['id'];

    // get order number and date
    $query_order = "SELECT * FROM orders WHERE id = $order_id";
    $order = $db->query($query_order)->fetchArray();

    // get order item 
    $query_product_order = "SELECT product_orders.*, products.name as product_name  FROM product_orders
            INNER JOIN products ON products.id = product_orders.product_id
            WHERE product_orders.order_id = $order_id
        ";
    $products = $db->query($query_product_order)->fetchAll();
?>

<a href="index.php" class="btn btn-success btn_goback">Go Back</a>

<section class="invoice">
    <h3 class="text-center">Tela Mart</h3>
    <h3 class="text-center">INVOICE</h3>
    <hr>
    <div class="row">
        <div class="col-6">
            <p class="text-left">Invoice number: <?php echo $order['order_code'] ?></p>
        </div>
        <div class="col-6">
            <p class="text-right">Date: <?php echo $order['created_at'] ?></p>
        </div>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No.</th>
                <th>Item Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Discount</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                // loop items
                $net_total_price = 0;
                foreach($products as $key => $pro){ 
            ?>
                <tr>
                    <td><?php echo (int)$key +1 ?></td>
                    <td><?php echo $pro['product_name'] ?></td>
                    <td><?php echo '$'.number_format($pro['item_price'], 2) ?></td>
                    <td><?php echo $pro['order_quantity'] ?></td>
                    <td><?php echo $pro['item_dicount'].'%' ?></td>
                    <td>
                        <?php 
                            $price_before_discount = ($pro['item_price'] * $pro['order_quantity']);
                            $price_discount = (($pro['item_price'] * $pro['order_quantity']) * $pro['item_dicount'])/100 ;
                            $total_price = $price_before_discount - $price_discount;
                            echo '$'. number_format($total_price, 2);
                            $net_total_price = $net_total_price + $total_price;
                        ?> 
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="5" class="text-right text-bold">Net Total Price (USD): </td>
                <td> <?php echo '$'.number_format($net_total_price, 2) ?> </td>
            </tr>
            <tr>
                <td colspan="5" class="text-right text-bold">Net Total Price (KH): </td>
                <td> <?php echo number_format($net_total_price*4100, 2).'៛' ?> </td>
            </tr>
        </tbody>
    </table>
</div>

<footer>
    <p class="text-center">Thank you for your supported!</p>
</footer>

<style>
    @media print {
        .btn_goback{
            display: none;
        }
    }
</style>

<script>
    //  auto laod print page  
    window.print();

</script>