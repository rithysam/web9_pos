<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');

    $query = "SELECT * FROM products WHERE active = 1";
    $products = $db->query($query)->fetchAll();

?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <!-- show message  -->
            <?php echo show_message(); ?>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Sale Product</h3>
             
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <div class="row p-3">
                    <div class="col-sm-12">
                        <section>
                            <form action="action/action_cart.php" method="post">
                                <div class="row">
                                    <div class="form-group col-sm-3">
                                        <label for="code">Barcode</label>
                                        <input type="text" name="code" autofocus class="form-control" placeholder="Scan barcode">
                                    </div>

                                    <div class="form-group col-sm-3">
                                        <label for="product_id">Product</label>
                                        <select name="product_id" id="product_id" class="form-control">
                                            <option value="">--- Select a product ---</option>
                                            <?php foreach($products as $pro){ ?>
                                                <option value="<?php echo $pro['id'] ?>"> <?php echo $pro['code'].'-'.$pro['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-sm-1">
                                        <label class="text-white">Add Label</label>
                                        <button class="btn btn-primary"><i class="fa fa-plus"></i> Add</button>
                                    </div>

                                </div>
                            </form>
                        </section>
                        <hr>
                        
                        <!-- selected product  -->
                        <section>
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="table-responsive">
                                        <table class="table table-hover text-nowrap">
                                            <thead>
                                                <tr>
                                                <th>No</th>
                                                <th>Code</th>
                                                <th>Name</th>
                                                <th>Price</th>
                                                <th>Quanity</th>
                                                <th>Discount</th>
                                                <th>Total</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if(isset($_SESSION['cart_item'])){
                                                    $net_total_price = 0;
                                                    foreach($_SESSION['cart_item'] as $key => $item){ 
                                                ?>
                                                    <tr>
                                                        <td><?php echo (int)$key+1 ?></td>
                                                        <td><?php echo $item['code'] ?></td>
                                                        <td><?php echo $item['name'] ?></td>
                                                        <td><?php echo '$'.number_format($item['sale_price'], 2);  ?></td>
                                                        <td><?php echo $item['quantity'] ?></td>
                                                        <td><?php echo $item['discount'] ?>%</td>
                                                        <td>
                                                            <?php 
                                                                $price_before_discount = ($item['sale_price'] * $item['quantity']);
                                                                $price_discount = (($item['sale_price'] * $item['quantity']) * $item['discount'])/100 ;
                                                                $total_price = $price_before_discount - $price_discount;
                                                                echo '$'. number_format($total_price, 2);
                                                                $net_total_price = $net_total_price + $total_price;
                                                            ?>
                                                        </td>

                                                        <td>
                                                            <button href="#" class="btn btn-xs btn-success btn_edit_quantity" title="Edit" data-key="<?php echo $key ?>" data-quantity="<?php echo $item['quantity'] ?>"  data-toggle="modal" ><i class="fa fa-edit"></i></button>
                                                            <a href="action/action_remove_item.php?key=<?php echo $key ?>" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                        </td>

                                                    </tr>
                                                
                                                <?php 
                                                    } 
                                                ?>

                                                    <tr>
                                                        <td colspan="6" class="text-right text-bold">Net Total Price (USD): </td>
                                                        <td colspan="2"> <?php echo '$'.number_format($net_total_price, 2) ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6" class="text-right text-bold">Net Total Price (KH): </td>
                                                        <td colspan="2"> <?php echo number_format($net_total_price*4100, 2).'៛' ?> </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            <a href="action/action_clear_cart.php"  class="btn btn-danger"> <i class="fa fa-times"></i> Cancel </a>
                                                        </td>
                                                        <td colspan="2">
                                                            <a href="action/action_order.php" class="btn btn-success"><i class="fa fa-save"></i> Save & Print</a>
                                                        </td>
                                                    </tr>

                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <form action="action/action_edit_quantity.php" method="post">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Quantity</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="quantity">Quantity</label>
                <input type="hidden" name="key" id="key" required>
                <input type="number" min="1" name="quantity" id="quantity" class="form-control" require>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </form>
  </div>
</div>



<?php 
    include ('../layouts/footer.php');
?>

<script>
    $(document).ready(function(){
        $(".btn_edit_quantity").click( function(){
            var key = $(this).attr("data-key");
            var quantity = $(this).attr("data-quantity");
            $("#quantity").val(quantity);
            $("#key").val(key);
            $("#exampleModal").modal('show');
        });
    })
</script>