<?php 
    include ('../../layouts/header.php');

    // check if form submited
    if(isset($_POST['code']) || isset($_POST['product_id'])){
        $code = $_POST['code'];
        $product_id = $_POST['product_id'];
        // verify data 
        if(!empty($code)){
            // get product if code not empty
            $query = "SELECT * FROM products WHERE active = 1 AND code = '$code' ";
        }elseif(!empty($product_id)){
             // get product if code is empty and product_id not empty
            $query = "SELECT * FROM products WHERE active = 1 AND id = '$product_id'";
        }else{
            $query = "SELECT * FROM products WHERE active = 1 AND id = null";
        }

        // check if exist in database 
        if($db->query($query)->numRows() == 1){
            // code here
            $product = $db->query($query)->fetchArray();

            // new item after clicked add button
            $cart_item = array([
                'id' => $product['id'],
                'code' => $product['code'],
                'name' => $product['name'],
                'sale_price' => $product['sale_price'],
                'discount' => $product['discount'],
                'quantity' => 1
            ]);

            // check if session have item 
            if(isset($_SESSION['cart_item']) && !empty($_SESSION['cart_item'])){
                
                $item_exist = false;

                // check item if already in cart session 
                foreach($_SESSION['cart_item'] as $k => $item){
                    if($product['id'] == $item['id']){
                        $item_exist = true;
                        // add quanity to exist item
                        $_SESSION['cart_item'][$k]['quantity'] = $_SESSION['cart_item'][$k]['quantity']+1;
                        break;
                    }
                }

                // new itme added to cart session 
                if(!$item_exist){
                    $_SESSION['cart_item'] = array_merge( $_SESSION['cart_item'], $cart_item);
                }

            }else{
                $_SESSION['cart_item'] = $cart_item;
            }

            header('Location: ../index.php');

        }else{
            message("No product is found!", 'error');
            header('Location: ../index.php');
        }
    }else{
        message("No product is found!", 'error');
        header('Location: ../index.php');
    }


?>