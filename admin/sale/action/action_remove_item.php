<?php
    include ('../../layouts/header.php');

    if(isset($_SESSION['cart_item'])){
        $key = $_GET['key'];
        if(count($_SESSION['cart_item']) > 1){
            unset($_SESSION['cart_item'][$key]); 
        }else{
            unset($_SESSION['cart_item']);
        }
    }

    header("Location: ../index.php");
?>