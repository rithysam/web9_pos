<?php 
    include ('../../layouts/header.php');
    // check if session cart exist
    if(isset($_SESSION['cart_item'])){
        $user_id = $_SESSION['user']['id'];
        $order_code = 'OD'.strtotime(date('Y-m-d H:i:s'));
        $query_order = "INSERT INTO orders(`order_code`, `user_id`) VALUES('$order_code', '$user_id')";
        $last_id = $db->query($query_order)->lastInsertID(); // insert to order and get inserted id;

        // check if get last_id (meant we inserted order)
        if($last_id){
            // loop item because 1 order can have multiple items
            foreach($_SESSION['cart_item'] as $key => $item){ 
                $quantity = $item['quantity'];
                $price = $item['sale_price'];
                $discount = $item['discount'];
                $product_id = $item['id'];
                $order_id = $last_id;
                $query = "INSERT INTO product_orders (`order_quantity`, `item_price`, `item_dicount`, `order_id`, `product_id`)
                        VALUES('$quantity', '$price', '$discount', '$order_id', '$product_id')
                    ";
                
                // check if order success then cut stock
                if($db->query($query)){
                    //get current tock
                    $query_current_stock = "SELECT quantity FROM products WHERE id = $product_id";
                    $current_stock = $db->query($query_current_stock)->fetchArray();
                    $next_quantity = $current_stock['quantity'] - $quantity;
                    
                    // upldate stock
                    $query_upldate_stock = "UPDATE products SET quantity = $next_quantity WHERE id = $product_id";
                    $db->query($query_upldate_stock);
                }

            }

            // clear cart item 
            unset($_SESSION['cart_item']);
            message('Order successfully');
            header("Location: ../invoice.php?id=$last_id");
        }

    }

?>



<!-- <script type="text/javascript" language="Javascript">window.open('<?php echo $url ?>');</script> -->