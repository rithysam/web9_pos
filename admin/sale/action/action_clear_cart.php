<?php 
    include ('../../layouts/header.php');

    if(isset($_SESSION['cart_item'])){
        unset($_SESSION['cart_item']);
        message('Cart items have cleared');
        header('Location: ../index.php');
    }else{
        message('No item to clear!', 'error');
        header('Location: ../index.php');
    }

?>