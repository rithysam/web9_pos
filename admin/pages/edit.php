<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
    $id = $_GET['id'];
    $query = "SELECT * FROM pages WHERE id = $id";
    $page = $db->query($query)->fetchArray();
?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Edit Page</h3>
              </div>
             <!-- show message  -->
            <?php echo show_message(); ?>
            
              <form action="action/action_edit.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="name">Name</label>
                      <input type="text" name="name" id="name" value="<?php echo $page['name'] ?>" class="form-control" required>
                    </div>

                    <div class="form-group col-sm-3">
                      <label for="photo">Photo</label>
                      <input type="file" name="photo" id="photo" class="form-control">
                     
                    </div>
                    <div class="col-sm-3">
                      <img src="<?php asset('assets/uploads/page/'.$page['photo']) ?>" alt="" width="50">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label for="">Description</label>
                      <textarea id="summernote" cols="1000" name="description" required>
                        <?php echo $page['description']?>
                      </textarea>
                    </div>
                    <!-- /.col-->
                  </div>

                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>



<?php 
    include ('../layouts/footer.php');
?>