<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <!-- show message  -->
            <?php echo show_message(); ?>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Page List</h3>

                <div class="card-tools">
                  <form>
                    <div class="input-group input-group-sm" style="width: 250px;">
                      <input type="text" name="search" value="<?php if(isset($_GET['search'])){echo $_GET['search'];} ?>" class="form-control float-right" placeholder="Search" required>

                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                        <?php
                          if(isset($_GET['search'])){
                        ?>
                          <a href="index.php" class="btn btn-default text-danger">X</a>
                        <?php
                          }
                        ?>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <div class="row p-3">
                    <div class="col-sm-12">
                        <a href="create.php" class="btn btn-primary">Create Page</a>
                    </div>
                </div>
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Photo</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if(isset($_GET['search'])){
                        $search = $_GET['search'];
                        $query = "SELECT pages.*  FROM pages
                          WHERE pages.name LIKE '%$search%'";
                      }else{
                        $query = "SELECT pages.* FROM pages";
                      }
                        $pages = $db->query($query)->fetchAll();
                    ?>
                    <?php foreach($pages as $key => $page){ ?>
                    <tr>
                      <td><?php echo $key +1; ?></td>
                      <td><?php echo $page['name']; ?></td>
                      <td>
                        <img src="<?php asset('assets/uploads/page/'.$page['photo']) ?>" width="50" alt="">
                      </td>
                      <td>
                          <?php 
                            if($page['active'] == 1){
                              echo '<span class="text-success">Active</span>';
                            }else{
                              echo '<span class="text-danger">In-active</span>';
                            }
                          ?>
                      </td>
                      <td>
                        <a href="action/action_toggle_disable.php?id=<?php echo $page['id'].'&status='.$page['active'] ?>" class="btn btn-info btn-xs"><i class="fa <?php if($page['active'] == 1) echo 'fa-eye'; else echo 'fa-eye-slash'; ?>"></i></a>
                        <a href="edit.php?id=<?php echo $page['id'] ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
                        <a href="action/action_delete.php?id=<?php echo $page['id'] ?>" onclick="return confirm('Are you sure?\nThis actoin cannot recovery!')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>