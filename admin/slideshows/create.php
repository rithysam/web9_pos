<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Create Slideshow</h3>
              </div>
             <!-- show message  -->
            <?php echo show_message(); ?>
            
              <form action="action/action_create.php" method="post" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="name">Name</label>
                      <input type="text" name="name" id="name" class="form-control" required>
                    </div>

                    <div class="form-group col-sm-6">
                      <label for="photo">Photo</label>
                      <input type="file" name="photo" id="photo" class="form-control" required>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>