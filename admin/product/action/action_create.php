<?php 
    include ('../../layouts/header.php');
    $code = $_POST['code'];
    $name = $_POST['name'];
    $category = $_POST['category'];
    $price = $_POST['price'];
    $sale_price = $_POST['sale_price'];
    $discount = $_POST['discount'];
    $quantity = $_POST['quantity'];
    
    // check fils exist 
    $file_name = "";
    if(!empty($_FILES['photo']['name'])){
        $upload_folder = '../../../assets/uploads/product/';
        $file_name = strtotime(date("Y-m-d H:i:s")).'.png';

        move_uploaded_file($_FILES['photo']['tmp_name'], $upload_folder.$file_name);
    }

    // check code if not exist
    $query_check = "SELECT * FROM products WHERE `code` = '$code' AND active = 1";
    $count = $db->query($query_check)->numRows();
    if($count > 0){
        message("Product barcode already exist.", "error");
        header("Location: ../create.php");
        
    }else{
        $query = "INSERT INTO products(
            `code`, 
            `name`, 
            `price`, 
            `sale_price`, 
            `quantity`,
            `discount`,
            `photo`,
            `category_id`
            ) 
        
            VALUES(
                '$code',
                '$name',
                '$price',
                '$sale_price',
                '$quantity',
                '$discount',
                '$file_name',
                '$category'
            )";
    
        if($db->query($query)){
            message("Data inserted successfully.");
            header("Location: ../index.php");
        }else{
            message("Data was not instered.", "error");
            header("Location: ../index.php");
        }
    }

   
?>