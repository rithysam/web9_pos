<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Create Product</h3>
              </div>
             <!-- show message  -->
            <?php echo show_message(); ?>
            
              <form action="action/action_create.php" method="post" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="name">Product Name</label>
                      <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <?php 
                        $query = "SELECT * FROM categories WHERE active = 1";
                        $categories = $db->query($query)->fetchAll();
                      ?>
                      <label for="category">Product Category</label>
                      <select name="category" id="category" class="form-control" required>
                        <option value="">--Choose a category--</option>
                        <?php foreach($categories as $cat){ ?>
                          <option value="<?php echo $cat['id'] ?>" ><?php echo $cat['name'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="price">Price ($)</label>
                      <input type="number" step="0.01" name="price" id="price" class="form-control" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="sale_price">Sale Price ($)</label>
                      <input type="number" step="0.01" name="sale_price" id="sale_price" class="form-control" required>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="quantity">Quantity</label>
                      <input type="number" name="quantity" id="quantity" class="form-control" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="discount">Discount</label>
                      <input type="number" step="0.01" name="discount" value="0" id="discount" class="form-control" required>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label for="code">Barcode</label>
                      <input type="text" name="code" id="code" class="form-control" required>
                    </div>
                    <div class="form-group col-sm-6">
                      <label for="photo">Photo</label>
                      <input type="file" name="photo" id="photo" class="form-control" required>
                    </div>
                  </div>


                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>