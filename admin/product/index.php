<?php 
    include ('../layouts/header.php');
    include ('../layouts/nav.php');
    include ('../layouts/sidebar.php');
?>

    <!-- html content here -->
    <div class="content-wrapper pt-3" style="min-height: 1299.69px;">

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <!-- show message  -->
            <?php echo show_message(); ?>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Product List</h3>

                <div class="card-tools">
                  <form>
                    <div class="input-group input-group-sm" style="width: 250px;">
                      <input type="text" name="search" value="<?php if(isset($_GET['search'])){echo $_GET['search'];} ?>" class="form-control float-right" placeholder="Search" required>

                      <div class="input-group-append">
                        <button type="submit" class="btn btn-default">
                          <i class="fas fa-search"></i>
                        </button>
                        <?php
                          if(isset($_GET['search'])){
                        ?>
                          <a href="index.php" class="btn btn-default text-danger">X</a>
                        <?php
                          }
                        ?>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <div class="row p-3">
                    <div class="col-sm-12">
                        <a href="create.php" class="btn btn-primary">Create Product</a>
                    </div>
                </div>
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Photo</th>
                      <th>Code</th>
                      <th>Name</th>
                      <th>Price</th>
                      <th>Sale Price</th>
                      <th>Quanity</th>
                      <th>Discount</th>
                      <th>Category</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if(isset($_GET['search'])){
                        $search = $_GET['search'];
                        $query = "SELECT products.*, categories.name as category_name  FROM products
                          INNER JOIN categories ON categories.id = products.category_id
                          WHERE products.active = 1 AND (products.name LIKE '%$search%' OR products.code LIKE '%$search%' OR categories.name LIKE '%$search%')";
                      }else{
                        $query = "SELECT products.*, categories.name as category_name  FROM products
                          INNER JOIN categories ON categories.id = products.category_id
                          WHERE products.active = 1";
                      }
                        $products = $db->query($query)->fetchAll();
                    ?>
                    <?php foreach($products as $key => $product){ ?>
                    <tr>
                      <td><?php echo $key +1; ?></td>
                      <td>
                        <img src="<?php asset('assets/uploads/product/'.$product['photo']) ?>" width="50" alt="">
                      </td>
                      <td><?php echo $product['code']; ?></td>
                      <td><?php echo $product['name']; ?></td>
                      <td><?php echo '$'.$product['price']; ?></td>
                      <td><?php echo '$'.$product['sale_price']; ?></td>
                      <td><?php echo $product['quantity']; ?></td>
                      <td><?php echo $product['discount'].'%'; ?></td>
                      <td><?php echo $product['category_name']; ?></td>
                      <td>
                        <a href="edit.php?id=<?php echo $product['id'] ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
                        <a href="action/action_delete.php?id=<?php echo $product['id'] ?>" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

<?php 
    include ('../layouts/footer.php');
?>